(defcustom org-latex--author-sep "%\n  \\and "
  "LaTeX separator for the list of authors"
  :group 'org-export-latex
  :type '(string :tag "Format string"))

(defun org-latex--format-author-email (data &optional thanks)
  "Format the author with her email using the format string, the
data is a tuple (Name, Email).  An optional `thanks` string can
be added."
  (let* ((author (car data))
         (email (cdr data))
         (name (s-split " " author))
         (first (car name))
         (last (s-trim (car (cdr name))))
         (plus (if thanks
                   (format "\\thanks{%s}" thanks)
                 "")))
    (format "%s \\textsc{%s}%s\\\\ \\texttt{%s}"
            first
            last
            plus
            email)))

(defun org-latex--format-author (author email)
  "Add a list of author(s) with their email(s), separated by
commas."
  (let* ((authors (mapcar 's-trim (s-split "," author)))
         (emails (mapcar 's-trim (s-split "," email)))
         (data (-zip authors emails))
         (rest (cdr data)))
    (concat
     "\\author{"
     ;; First author has thanks
     (org-latex--format-author-email (car data) "Corresponding author.")
     ;; Rest of the authors are added after
     (if rest
         (concat
          org-latex--author-sep
          (mapconcat 'org-latex--format-author-email
                     (cdr data)
                     org-latex--author-sep)))
     "}")))

(org-latex--format-author "A M, M F" "1, 2")
(org-latex--format-author "A M" "1")
